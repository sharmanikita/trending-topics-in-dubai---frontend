import Controller from '@ember/controller';
import { inject } from '@ember/service';

export default Controller.extend({
  socketService: inject('websockets'),

  init: function () {
    this._super(...arguments);

    var socket = this.get('socketService').socketFor('ws://myapp.here.com:8001/ts');

    socket.on('open', this.openHandler, this);
    socket.on('message', this.messageHandler, this);
    socket.on('close', this.closeHandler, this);
    socket.on('error', this.errorHandler, this);
  },

  openHandler: function(socketEvent) {
    console.log('On open has been called! type = ' + socketEvent.type);
  },

  closeHandler: function(socketEvent) {
    console.log('On close has been called! type = ' + socketEvent.type);
  },

  messageHandler: function(socketEvent) {
    var tweet = JSON.parse(socketEvent.data);
    console.log('Got tweet from ' + tweet.author);
  },

  errorHandler: function(socketEvent) {
    console.log('On error has been called! :-( type = ' + socketEvent.type);
  }
});
