import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('topics', { path: '/' });
  this.route('tweets', function(){
    this.route('tweets', { path: '/tweets/:id' });
  });
});

export default Router;
