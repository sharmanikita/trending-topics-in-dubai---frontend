import DS from 'ember-data';
const { Model } = DS;

export default Model.extend({
  name: DS.attr(),
  url: DS.attr(),
  promoted_content: DS.attr(),
  query: DS.attr(),
  tweet_volume: DS.attr()
});
