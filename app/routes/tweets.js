import Route from '@ember/routing/route';

export default Route.extend({
  queryParams: {
    name: ''
  },

  model(params) {
    return this.store.query('tweet', params);
  }
});
