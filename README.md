This application displays a list of Top 25 trending topics in Dubai and to displays latest tweets for any selected topics.

Installation
  
* npm install -g ember-cli
* npm install -g bower
* git clone
* npm install
* ember serve --proxy http://127.0.0.1:8001
Please note the proxy flag in the last command, this expects your node backed to be listening on port 8001. See below for details.

Backend
The `https://gitlab.com/sharmanikita/trending-topics-in-dubai---backend` application can be used as the backed for this example.
